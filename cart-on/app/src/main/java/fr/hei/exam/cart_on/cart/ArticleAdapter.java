package fr.hei.exam.cart_on.cart;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import fr.hei.exam.cart_on.R;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ViewHolder>{
    List<Article> articleList;

    public ArticleAdapter(List<Article> articleList) {
        this.articleList = articleList;
    }

    @NonNull
    @Override
    public ArticleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_article_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleAdapter.ViewHolder holder, int position) {
        holder.name.setText(articleList.get(position).getName());
        holder.brand.setText(articleList.get(position).getBrand());
        holder.quantity.setText("x" + Integer.toString(articleList.get(position).getQuantity())); // need to transform int to string
        holder.price.setText(Float.toString(articleList.get(position).getPrice()) + "€"); // need to transform float to string
        Picasso.get().load(articleList.get(position).getImageUri()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        if (articleList!=null) {
            return articleList.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView name, brand, price, quantity;
        ImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            name = view.findViewById(R.id.single_article_name);
            brand = view.findViewById(R.id.single_article_brand);
            price = view.findViewById(R.id.single_article_price);
            quantity = view.findViewById(R.id.single_article_quantity);
            image = view.findViewById(R.id.single_article_image);
        }
    }
}
