package fr.hei.exam.cart_on.cart;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class CartViewModel extends ViewModel {

    MutableLiveData<List<Article>> cartMutableLiveData;
    FirebaseFirestore mFirestore;
    CartRepository cartRepository;

    public CartViewModel() {
        mFirestore = FirebaseFirestore.getInstance();

        cartRepository = new CartRepository();
        cartMutableLiveData = cartRepository.getCartMutableLiveData("GFi1KRdzltlWcdAhBh1n");
    }

    public MutableLiveData<List<Article>> getCartMutableLiveDataL() {
        return cartMutableLiveData;
    }
}