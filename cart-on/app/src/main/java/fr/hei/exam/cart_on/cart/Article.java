package fr.hei.exam.cart_on.cart;

public class Article {
    private String name;
    private String brand;
    private String imageUri;
    private int quantity;
    private float price;

    public Article() {

    }

    public Article(String name, String brand, String imageUri, int quantity, float price) {
        this.name = name;
        this.brand = brand;
        this.quantity= quantity;
        this.imageUri = imageUri;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    @Override
    public String toString() {
        return "Article (" + quantity + "): " + name + ", " + brand;
    }
}
