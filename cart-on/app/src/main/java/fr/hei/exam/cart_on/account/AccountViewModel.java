package fr.hei.exam.cart_on.account;

import android.net.Uri;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class AccountViewModel extends ViewModel {
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    private final MutableLiveData<String> mName;
    private final MutableLiveData<String> mEmail;
    private final MutableLiveData<Uri> mAvatarUri;

    public AccountViewModel() {
        mName = new MutableLiveData<>();
        mEmail = new MutableLiveData<>();
        mAvatarUri = new MutableLiveData<>();

        mName.setValue(user.getDisplayName());
        mEmail.setValue(user.getEmail());
        mAvatarUri.setValue(user.getPhotoUrl());
    }

    public LiveData<String> getName() {
        return mName;
    }

    public LiveData<String> getEmail() {
        return mEmail;
    }

    public LiveData<Uri> getAvatarUri() {
        return mAvatarUri;
    }
}