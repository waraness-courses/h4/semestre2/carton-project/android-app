package fr.hei.exam.cart_on.account;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import fr.hei.exam.cart_on.AuthActivity;
import fr.hei.exam.cart_on.databinding.FragmentAccountBinding;

public class AccountFragment extends Fragment {

    private FragmentAccountBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AccountViewModel accountViewModel = new ViewModelProvider(this).get(AccountViewModel.class);

        binding = FragmentAccountBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        // Get current user info
        final TextView name = binding.accountName;
        accountViewModel.getName().observe(getViewLifecycleOwner(), name::setText);
        final TextView email = binding.accountEmail;
        accountViewModel.getEmail().observe(getViewLifecycleOwner(), email::setText);
        final ImageView avatar = binding.accountAvatar;
        Picasso.get().load(accountViewModel.getAvatarUri().getValue()).into(avatar);

        // Logout button
        Button button = binding.accountLogOutButton;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Logout
                FirebaseAuth.getInstance().signOut();
                // Redirect to auth activity
                Intent authIntent = new Intent(getContext(), AuthActivity.class);
                startActivity(authIntent);
            }
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}