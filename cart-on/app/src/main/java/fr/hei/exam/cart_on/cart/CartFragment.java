package fr.hei.exam.cart_on.cart;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import fr.hei.exam.cart_on.databinding.FragmentCartBinding;


public class CartFragment extends Fragment {
    private FragmentCartBinding binding;

    private CartViewModel cartViewModel;
    private RecyclerView articleRecycler;
    private ArticleAdapter articleAdapter;
    ProgressDialog progressDialog;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        CartViewModel dashboardViewModel =
                new ViewModelProvider(this).get(CartViewModel.class);

        binding = FragmentCartBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        // Define ViewModel
        cartViewModel = new ViewModelProvider(this).get(CartViewModel.class);
        progressDialog =new ProgressDialog(getContext());
        progressDialog.setTitle("Loading ...");
        progressDialog.show();

        articleRecycler = binding.cartRecyclerView;
        articleRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        articleRecycler.setHasFixedSize(true);

        // Get article list through ViewModel
        cartViewModel.getCartMutableLiveDataL().observe((LifecycleOwner) getContext(), articleList -> {
            progressDialog.dismiss();
            articleAdapter = new ArticleAdapter(articleList);
            articleRecycler.setAdapter(articleAdapter);
            articleAdapter.notifyDataSetChanged();

        // Get cart total
        TextView total = binding.cartTotal;
        total.setText(Float.toString(getCartTotal()) + "€");
        });
        return root;
    }

    private float getCartTotal() {
        float total = 0;
        if (cartViewModel.getCartMutableLiveDataL() != null) {
            for (int i = 0; i < cartViewModel.getCartMutableLiveDataL().getValue().size(); i++) {
                Article article = cartViewModel.getCartMutableLiveDataL().getValue().get(i);
                total += article.getQuantity() * article.getPrice();
            }
        }
        return total;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}