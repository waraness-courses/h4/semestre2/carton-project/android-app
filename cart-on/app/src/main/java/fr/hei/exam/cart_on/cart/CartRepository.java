package fr.hei.exam.cart_on.cart;

import android.util.Log;

import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CartRepository {
    private static final String TAG = "CartRepository";

    MutableLiveData<List<Article>> cartMutableLiveData;
    FirebaseFirestore mFirestore;
    MutableLiveData<Article> articleMutableLiveData;

    private String mCartToListen;
    private String mCartDataJson;

    public CartRepository() {
        this.cartMutableLiveData = new MutableLiveData<>();

        mFirestore = FirebaseFirestore.getInstance(); // Define Firestore
        articleMutableLiveData = new MutableLiveData<>(); // Define article
    }

    public MutableLiveData<List<Article>> getCartMutableLiveData(String cartToListen) {
        this.mCartToListen = cartToListen;

        // Listen cart content
        DocumentReference cartRef = mFirestore.collection("carts").document(mCartToListen);
        cartRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                if (error != null) {
                    Log.d(TAG, "Failed to get cart content: " + error);
                    return;
                }

                if (value != null && value.exists()) {
                    mCartDataJson = value.getData().toString();
                    Log.d(TAG, "Successfully get cart content: " + mCartDataJson);

                    // Parse Json response
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(mCartDataJson);
                        JSONArray articlesArray = new JSONArray(jsonObject.getString("articles"));

                        List<Article> articleList = new ArrayList<>();
                        for (int i = 0; i < articlesArray.length(); i++) {
                            JSONObject articleObject = articlesArray.getJSONObject(i);
                            String articleReference = articleObject.getString("reference");
                            int articleQuantity = articleObject.getInt("quantity");

                            // Get data from each article
                            DocumentReference cartRef = mFirestore.collection("articles").document(articleReference);
                            cartRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    Article article = documentSnapshot.toObject(Article.class);
                                    article.setQuantity(articleQuantity);
                                    Log.d(TAG, article.toString());

                                    articleList.add(article);
                                    cartMutableLiveData.postValue(articleList);
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, e.getMessage());
                    }

                } else {
                    mCartDataJson = "";
                    Log.d(TAG, "Success: Current data null");
                }
            }
        });

        return cartMutableLiveData;
    }
}