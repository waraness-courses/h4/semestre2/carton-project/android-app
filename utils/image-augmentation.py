

import imgaug.augmenters as iaa
import cv2
import glob

# Load Dataset
images = []
images_path = glob.glob("./epinards/*.jpg")
for img_path in images_path:
    img = cv2.imread(img_path)
    images.append(img)

# Image Augmentation
augmentation = iaa.Sequential([
    # Flip
    iaa.Fliplr(0.5),
    iaa.Flipud(0.5),

    # Affine
    iaa.Affine(translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)},
               rotate=(-30, 30),
               scale=(0.5, 1.5)),

    # Multiply
    iaa.Multiply((0.8, 1.2)),

    # Linearcontrast
    iaa.LinearContrast((0.6, 1.4)),

    # Perform methods below only sometimes
    iaa.Sometimes(0.5,
        # aussianBlur
        iaa.GaussianBlur((0.0, 3.0))
        )

])


# Save Images
augmented_images = augmentation(images=images)
i = 0;
for img in augmented_images:
    print("Saving image", i)
    cv2.imwrite("./output/aaa" + str(i) + ".jpg", img)
    i = i + 1